/**
 * Created by Owner on 6/23/2017.
 */

import React, { Component } from 'react'
// needed for JSX transpile

class SearchBar extends Component {
  constructor(props) {
    super(props)

    this.state = { term: '' }
  }

  render() {
    return (
      <div className="search-bar">
      <input
        value={this.state.term}
        onChange={eventObject => this.onInputChange(eventObject.target.value)}
      />
      </div>
    )
  }

  onInputChange(term) {
    this.setState({term: term})
    this.props.onSearchTermChange(term)
  }
}

export default SearchBar
// This must match the component (function/class) name


//all HTML DOM elements emit change events

//constructor function/method is the only one that gets auto-called on every new instance

// super is a constructor insidte the constructor method

//state is a plain JS object. It's on all class based components