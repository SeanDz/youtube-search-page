/**
 * Created by Owner on 6/23/2017.
 */

// no need for state. Function component

import React from 'react'
import VideoListItem from './video_list_item'

const VideoList = (props) => {
  const videoItems = props.videos.map((video) => {
    return (
      <VideoListItem
        onVideoSelect={props.onVideoSelect}
    key={video.etag}
    video={video} />
    )
  })


  return (
    <ul className="col-md-4 list-group">
      {videoItems}
    </ul>
  )
}

//props object passes from the parent to child as an argument (for functional components). It comes in as props


export default VideoList