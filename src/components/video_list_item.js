/**
 * Created by Owner on 6/23/2017.
 */

import React from 'react'


//props object. But instead we ID the specific props we want. They are now assigned as constants for their exact namespaces
const VideoListItem = ({video, onVideoSelect}) => {
  const imageUrl = video.snippet.thumbnails.default.url
  return (
    <li onClick={() => onVideoSelect(video)} className="list-group-item">
          <div className="video-list media">
            <div className="media-left">
              <img alt="" className="media-object" src={imageUrl} />
            </div>

            <div className="media-body">
              <div className="media-heading">{video.snippet.title}</div>
            </div>
          </div>
    </li>
  )
}

export default VideoListItem

// const videoArg = props.videoArg -- exactly the same as {videoArg}
