/**
 * Created by Owner on 6/22/2017.
 */

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import YTSearch from 'youtube-api-search'
import SearchBar from './components/search_bar'
//babel needs an exact file ref for components you write. Libraries are namespaced though (react, react-dom)
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'
import _ from 'lodash'
const API_KEY = 'AIzaSyCvmeprbPCcnMmQfOFlGpAOAoFkoXii_2s'

// Create a new component. Component should produce HTML

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      videos: [],
      selectedVideo: null
    }
    this.videoSearch('surfboards')
  }
  // ES6 compacted videos: videos

  videoSearch(term) {
    YTSearch({key: API_KEY, term: term}, (videos) => {
      this.setState({
        videos: videos,
        selectedVideo: videos[0]
      })
    })
  }

  render() {
    const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 500)

    return (
      <div>
        <SearchBar onSearchTermChange={videoSearch} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList
          onVideoSelect= {
            (selectedVideo) => this.setState({selectedVideo})
          }
          videos={this.state.videos}
        />
      </div>
    )
  }
}
//we've create the class. Now to instantiate it

// Take this component's HTML and put it on the page (DOM)

ReactDOM.render(<App />, document.querySelector('.container'))
// 2nd arg is the target DOM node